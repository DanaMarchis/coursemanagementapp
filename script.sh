sudo service postgresql restart
sleep 2
sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD 'password';"
sudo -u postgres psql -c "CREATE USER \"ec2-user\" SUPERUSER;"
sudo -u postgres psql -c "ALTER USER \"ec2-user\" WITH PASSWORD 'password';"

sleep 2
gem install bundler
sleep 1
gem install pg -v '1.1.4'
sleep 1
bundle install
sleep 1
rails generate ckeditor:install --orm=active_record --backend=carrierwave
sleep 1

createdb course_management_app_development
rake db:migrate
rake db:seed
