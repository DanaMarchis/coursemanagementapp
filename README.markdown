# Course Management App - LPE2019

It is a Rails 5 app with Ruby 2.6.0
This app uses the StarterKitRails. It is also the starter kit for LPE2019 event.

## Running on your Machine
Get the source:

    $ git clone git@gitlab.com:DanaMarchis/coursemanagementapp.git course_management_app

    $ cd course_management_app

Setup rvmrc (assuming you are using TextMate):

    $ mate .rvmrc

Paste the following content and save:

    $ rvm use 2.6.0@course_management_app --create

Then:

    $ cd ..
    $ cd course_management_app

I am using postgreSQL for the underlying database. You will need to setup your own config/database.yml. A sample file:

    $ development:
    $   adapter:    postgresql
    $   host:       localhost
    $   database:   course_management_app_development
    $   timeout:    5000
    $   encoding:   utf8
    $   pool:       5

Run migrations:

    $ createdb course_management_app_development
    $ rake db:migrate
    $ rake db:seed

Start the server:

    $ rails s
